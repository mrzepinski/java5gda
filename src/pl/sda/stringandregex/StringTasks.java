package pl.sda.stringandregex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringTasks {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        String search = scanner.nextLine();

    }

    private static String task2(String a, String b) {
        String aPrim = a.substring(1, a.length());
        String bPrim = b.substring(1);
        return aPrim + bPrim;
    }

    private static void task3_1(String text, String search) {
        if (text.contains(search)) {
            int counter = 0;
            int index = text.indexOf(search);
            while (index >= 0) {
                text = text.substring(index + search.length());
                index = text.indexOf(search);
                counter++;
            }
            System.out.println(search + ": " + counter);
        } else {
            System.out.println(text + " - does not contain: " + search);
        }
    }

    private static void task3_2(String text, String search) {
        int counter = 0;
        while (text.contains(search)) {
            text = text.substring(text.indexOf(search) + search.length());
            counter++;
        }
        System.out.println(search + ": " + counter);
    }

    private static void task3_3(String text, String search) {
        Pattern pattern = Pattern.compile(search);
        Matcher matcher = pattern.matcher(text);
        System.out.println(search + ": " + matcher.groupCount());
    }

    private static boolean task4(String text) {
        StringBuilder stringBuilder = new StringBuilder(text);
        stringBuilder.reverse();
        String reverse = stringBuilder.toString();
        return text.equalsIgnoreCase(reverse);
    }
}
