package pl.sda.stringandregex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexExamples {
    public static void main(String[] args) {
        String text1 = "abc 123 def";
        String text2 = "Hello Java! I love Java!";
        String text3 = "abcccc";

        Pattern pattern = Pattern.compile("([a-z])");
        Matcher matcher = pattern.matcher(text1);
        if (matcher.find()) {
            System.out.println(matcher.group());
        }

        String newText1 = text2.replaceAll("Java", "JavaScript");
        System.out.println(newText1);

        String newText2 = text2.replaceAll("\\s", "");
        System.out.println(newText2);

        String reg1 = "^abc.";
        System.out.println(text3 + " matches: `" + reg1 + "`? - " + text3.matches(reg1));

        String reg2 = "^abc.*";
        System.out.println(text3 + " matches: `" + reg2 + "`? - " + text3.matches(reg2));

        String reg3 = "^abc+";
        System.out.println(text3 + " matches: `" + reg3 + "`? - " + text3.matches(reg3));

        String reg4 = "^abc?";
        System.out.println(text3 + " matches: `" + reg4 + "`? - " + text3.matches(reg4));

        String reg5 = "^abc?c*";
        System.out.println(text3 + " matches: `" + reg5 + "`? - " + text3.matches(reg5));
    }
}
