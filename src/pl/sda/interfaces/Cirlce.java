package pl.sda.interfaces;

public class Cirlce implements Figure {
    private final double r;

    public Cirlce(int r) {
        this.r = r;
    }

    @Override
    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * r;
    }
}
